module "acm" {
  source                                      = "terraform-aws-modules/acm/aws"
  version                                     = "4.3.2"
  for_each                                    = var.acm_conf
  domain_name                                 = each.key
  create_route53_records_only                 = try(each.value.create_route53_records_only, var.create_route53_records_only)
  validate_certificate                        = try(each.value.validate_certificate, var.validate_certificate)
  validation_allow_overwrite_records          = try(each.value.validation_allow_overwrite_records, var.validation_allow_overwrite_records)
  wait_for_validation                         = try(each.value.wait_for_validation, var.wait_for_validation)
  validation_timeout                          = try(each.value.validation_timeout, var.validation_timeout)
  certificate_transparency_logging_preference = try(each.value.certificate_transparency_logging_preference, var.certificate_transparency_logging_preference)
  subject_alternative_names                   = try(each.value.subject_alternative_names, var.subject_alternative_names)
  validation_method                           = try(each.value.validation_method, var.validation_method)
  validation_option                           = try(each.value.validation_option, var.validation_option)
  create_route53_records                      = try(each.value.create_route53_records, var.create_route53_records)
  validation_record_fqdns                     = try(each.value.validation_record_fqdns, var.validation_record_fqdns)
  zone_id                                     = try(each.value.zone_id, var.zone_id)
  tags                                        = try(each.value.tags, var.tags)
  dns_ttl                                     = try(each.value.dns_ttl, var.dns_ttl)
  acm_certificate_domain_validation_options   = try(each.value.acm_certificate_domain_validation_options, var.acm_certificate_domain_validation_options)
  distinct_domain_names                       = try(each.value.distinct_domain_names, var.distinct_domain_names)
  key_algorithm                               = try(each.value.key_algorithm, var.key_algorithm)
}